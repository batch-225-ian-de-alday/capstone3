import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { NavLink, Link } from 'react-router-dom'
import Container from 'react-bootstrap/Container';
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'


export default function AppNavbar() {

    const { user } = useContext(UserContext)

    // console.log(user)

    return (
        <Navbar bg="dark" expand="lg" variant='dark' style={{ height: '80px' }}>
            <Container fluid>
                <Navbar.Brand as={Link} to="/">CID Skin Shop</Navbar.Brand>

                <Nav className="d-flex">
                    {(user.id) ?
                        <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                        :
                        <>
                            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                        </>
                    }
                </Nav>
            </Container>
        </Navbar>
    );
}