import { Button, ButtonGroup, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {
  return (
    <Row>
      <Col className="text-center">
        <h1 className='text-light pt-3'>Admin Dashboard</h1>
        <ButtonGroup className='me-2' aria-label="First group">
          <Button  as={Link} to='/addProduct' variant="primary">
            Add New Product
          </Button>
        </ButtonGroup>

        <ButtonGroup className='me-2' aria-label="Second group">
          <Button as={Link} to="/allOrders" variant="success">
            Show User Orders
          </Button>
        </ButtonGroup>
      </Col>
    </Row>
  );
}