import Carousel from 'react-bootstrap/Carousel';
import dyroth from '../images/dyroth.jpg';
import estes from '../images/Estes.jpg';
import fredrinn from '../images/fredrinn.jpg';
import laylaAndSaber from '../images/laylaAndsaber.jpg';
import lesley from '../images/lesley.jpg';
import odette from '../images/oddette.jpg';

function HomePagePicture() {
  return (
    <Carousel fade style={{ backgroundColor: '#000' }}>

      <Carousel.Item>
        <img
          className="d-block w-100 imageDyroth"
          src={dyroth}
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100 imageFredrinn"
          src={fredrinn}
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100 imageDyroth"
          src={estes}
          alt="Third slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100 imageDyroth"
          src={laylaAndSaber}
          alt="Third slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100 imageDyroth"
          src={lesley}
          alt="Third slide"
        />
      </Carousel.Item>
    </Carousel>
  );
}

export default HomePagePicture;