import Table from 'react-bootstrap/Table';
import { Link } from "react-router-dom";
import AdminDashboard from '../pages/AdminDashboard';

export default function ProductsTable(prod) {


  // const { name, description, isActive, price, image, _id } = productsProp.product;
  // // console.log(props.product)

  console.log(prod)

  return (
    <Table striped bordered hover variant="dark">
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Availability</th>
          <th>Actions</th>
        </tr>
      </thead>
    </Table>
  );
}
