import { Card, CardImg, Container } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function AllOrders(props) {

  const { name, totalAmount, products } = props.orderProp;
  
  console.log(props.orderProp.totalAmount);
  console.log(props.orderProp);

  // object destructuring if you have a nested array inside an object
//   console.log(props.orderProp.products[1].quantity);



return (
    <>
    <div className="backgroundIan">
    <Container className="p-3 g-1">
      <Card bg="dark"style={{ marginTop: "1rem", marginBottom: "1.5rem"}}>
        <Card.Body className="text-light">
          <Card.Title>{name}</Card.Title>
          <Card.Title>Orders:</Card.Title>
          {products.map((product, index) => (
            <Card style={{ marginTop: "1rem", backgroundColor: "rgb(255,255,255)" }} key={index}>
              <Card.Body className="text-dark">
                <Card.Subtitle>Product Name:</Card.Subtitle>
                <Card.Text>{product.product}</Card.Text>
                <Card.Subtitle>Quantity: {product.quantity}</Card.Subtitle>
                <br></br>
                <Card.Subtitle>Amount: {product.amount}</Card.Subtitle>
              </Card.Body>
            </Card>
          ))}
          <br></br>
          <Card.Title>Total Amount:</Card.Title>
          <Card.Title>{totalAmount}</Card.Title>
        </Card.Body>
      </Card>
    </Container>
    </div>
    </>
  );
}