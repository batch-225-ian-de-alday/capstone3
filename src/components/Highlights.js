import { Row, Col, Card, CardImg, Container } from 'react-bootstrap';

export default function Highlights() {
    return (
        <Container fluid className="mt-4 mx-auto" style={{ width: "90%" }}>
            <h1 className='text-light divider'>Upcoming Mobile Legend Hero Skins</h1>

            <Row className="mt-2 mb-3">
                <Col xs={12} md={6}>
                    <Card className="higlightsCard p-3 h-100">
                        <Card.Body className="d-flex flex-column higlightsCard">
                            <Card.Title className='higlightsCard'>
                                <h2 className='higlightsCard'>MLBB 515 Event featuring Rafaela skin</h2>
                            </Card.Title>
                            <Card.Text className="flex-grow-1 higlightsCard">
                                One of the biggest events in MLBB, the 515 event, will also be making its annual return for a limited time featuring a new skin for Rafaela. Players also have a chance to earn promo diamonds which can be used to reduce the price of most skins and items in the in-game shop.
                            </Card.Text>
                            <CardImg className="mt-auto higlightsCard" src="https://gumlet.assettype.com/afkgaming%2F2022-12%2F8904ab97-1f5d-4462-868d-7fdbee70bae1%2FMLBB_2023_events_and_skins.jpg?auto=format%2Ccompress&dpr=1.0&w=900" />
                        </Card.Body>
                    </Card>
                </Col>
                <Col xs={12} md={6}>
                    <Card className="cardHighlight p-3 h-100">
                        <Card.Body className="d-flex flex-column higlightsCard">
                            <Card.Title className='higlightsCard'>
                                <h2 className='higlightsCard'>Dyrroth Naraka Flame skin</h2>
                            </Card.Title>
                            <Card.Text className="flex-grow-1 higlightsCard">
                                The new Collector tier skin Naraka Flame Dyrroth is said to be released on 6th January 2023 and can be acquired through a draw event.
                            </Card.Text>
                            <CardImg className="mt-auto" src="https://gumlet.assettype.com/afkgaming%2F2022-12%2F57ced484-8e1e-4782-9a84-90031722e6da%2Fdyrothh.PNG?auto=format%2Ccompress&dpr=1.0&w=900" />
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}