import { Card, CardImg, Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ShopCards(product) {

    const { name, description, price, image, _id } = product.shopProp;

    // console.log(product.shopProp);


    return (
        <Container className="p-3 g-1">
              <Card>
                <Card.Body>
                  <Card.Title>{name}</Card.Title>
                  <Card.Text>Description: {description}</Card.Text>
                  <Card.Text>Price: PHP {price}</Card.Text>
                  <Link className="btn btn-primary" to={`/viewProduct/${_id}`}>
                    Details
                  </Link>
                </Card.Body>
              </Card>
        </Container>
      );
    }