import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { NavLink, Link } from 'react-router-dom'
import { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext'

// import NavDropdown from 'react-bootstrap/NavDropdown';
// import Nav from 'react-bootstrap/Form';
// import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';


export default function AppNavbar() {

	const { user } = useContext(UserContext)

	// console.log(user)

	return (
		<Navbar bg="dark" expand="lg" variant='dark' style={{ height: '80px' }}>
			<Container fluid>
				<Navbar.Brand as={Link} to="/">Mobile Legend</Navbar.Brand>
				<Navbar.Toggle aria-controls="navbarScroll" />
				<Navbar.Collapse id="navbarScroll">
					<Nav
						className="me-auto my-2 my-lg-0"
						style={{ maxHeight: '100px' }}
						navbarScroll
					>
						<Nav.Link as={NavLink} to="/">Home</Nav.Link>
						<Nav.Link as={NavLink} to="/shop">Shop</Nav.Link>


						{/* // check if the user is an admin */}
						{(user.isAdmin) ?
							<>
								<Nav.Link as={NavLink} to="/adminDashboard">Dashboard</Nav.Link>
							</>
							:
							null}

						{(user.id && !user.isAdmin) ?
							<>
								{/* <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link> */}
								<Nav.Link as={NavLink} to="/user-orders">Orders</Nav.Link>
							</>
							:
							null}


					</Nav>
					<Nav className="d-flex">
						{(user.id) ?
							<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
							:
							<>
								<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
								<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}