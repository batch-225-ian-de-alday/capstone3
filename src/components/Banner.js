// Destructure importation
import { Button, Row, Col, Container} from 'react-bootstrap'
import {Link} from 'react-router-dom';


export default function Banner() {
    return (
        <Container fluid className="mt-4 text-center text-light">
        <Row>
            <Col>
                <h1>Get your Exclusive Skins with us</h1>
                <p>Play more confident than others.</p>

                <Button as={Link} to='/shop' variant="success">Shop now!</Button>
            </Col>
        </Row>
        </Container>
    )
}