import { useState, useEffect, useContext } from 'react'
import { Navigate, useLocation } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Logout() {
  const { unsetUser, setUser } = useContext(UserContext)
  const [confirmed, setConfirmed] = useState(false)
//   const location = useLocation()

  useEffect(() => {
    if (!confirmed) {
      Swal.fire({
        title: "You're about to logout",
        icon: 'warning',
        text: 'Kindly press OK to confirm',
        showCancelButton: true,
        confirmButtonText: 'OK',
        cancelButtonText: 'Cancel',
        allowOutsideClick: false
      }).then((result) => {
        if (result.isConfirmed) {
          unsetUser()
          setUser({
            id: null,
            isAdmin: null
          })
          setConfirmed(true)
        } else {
          // Redirect the user to the previous page if they cancel the logout process
          window.history.back()
        }
      })
    }
  }, [confirmed, unsetUser, setUser, setConfirmed])

  return confirmed ? <Navigate to="/login" /> : null
}