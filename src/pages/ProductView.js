import { useState, useEffect, useContext } from 'react';
import { Container as div, Card, Button, Row, Col, CardImg, Container } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import Leomord from '../images/LeomordEpicSkin.jpg'


export default function ProductView(product) {

	// Gets the courseId from the URL of the route that this component is connected to. '/courses/:courseId'
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState("");

	const checkout = () => {
        Swal.fire({
          title: "Enter Quantity",
          html: `
            <input id="swal-input-quantity" class="swal2-input" type="number" placeholder="Quantity">
          `,
          showCancelButton: true,
          confirmButtonText: "Confirm",
          showLoaderOnConfirm: true,
          preConfirm: () => {
            const quantity = document.getElementById('swal-input-quantity').value;
            // const address = document.getElementById('swal-input-address').value;
            return { quantity};
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
          if (result.isConfirmed) {
            const reqBody = {
              quantity: result.value.quantity
            };
            fetch(`${process.env.REACT_APP_API_URL}/orders/checkout/${productId}`, {
              method: 'POST',
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token')}`
              },
              body: JSON.stringify(reqBody)
            })
            .then(response => response.json())
            .then(result => {

            console.log(result)

              if (result) {
                Swal.fire({
                  title: "Success!",
                  icon: "success",
                  text: "Your order has been created successfully!"
                })
                navigate('/shop')
              } else {
                Swal.fire({
                  title: "Something went wrong!",
                  icon: "error",
                  text: "Please try again."
                })
              }
            })
          }
        })
      }

	useEffect(() => {
        const fetchData = async () => {
            let url;
            if (user.id === null) {
                url = `${process.env.REACT_APP_API_URL}/products/details/no-token/${productId}`;
            } else {
                url = `${process.env.REACT_APP_API_URL}/products/details/${productId}`;
            }
            const response = await fetch(url, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'authorization': `Bearer ${localStorage.getItem('token')}`
                }
            });
            const result = await response.json();
            setName(result.name);
            setDescription(result.description);
            setPrice(result.price);
            setImage(result.image);
        };
    
        fetchData();
    }, [user.id, productId]);

	return(
	<>
		<div className="p-4 backgroundIan">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
                        <Card.Text className='cardTitle'>{name}</Card.Text>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							{/*<CardImg top src={price}/>*/}
							<Card.Text>PhP {price}</Card.Text>
							<CardImg top src="https://i.pinimg.com/originals/66/22/ab/6622ab37c6db6ac166dfec760a2f2939.gif" style={{ width: '84%' }}/>

                        
                            <div className='mt-2'>
							{	user.id !== null ? 
                            
									<Button variant="primary" onClick={() => checkout(productId)} disabled={user.isAdmin}>Order Now</Button>
								:
									<Link className="btn btn-danger btn-block" to="/login">Log in to Order</Link>
							}
                            </div>
							
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</div>
	</>
	)
}