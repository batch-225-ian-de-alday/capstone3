import AdminBanner from '../components/AdminBanner';
import { useState, useEffect } from 'react';
import Loading from '../components/Loading';
import { Button, Container, Row } from 'react-bootstrap';
import { Table } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Link, Navigate } from 'react-router-dom';


export default function AdminDashboard() {

    const [allProducts, setAllProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [navigateToDashboard, setNavigateToDashboard] = useState(false); // new state variable

    // use for fetching all products
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);
                setAllProducts(result);
                setIsLoading(false);
            })
            .catch(console.error);
    }, []);



    const handleEnable = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, enable it!'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(`${process.env.REACT_APP_API_URL}/products/activate/${id}`, {
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        isActive: true
                    })
                })
                    .then((response) => response.json())
                    .then((result) => {
                        console.log(result);
                        setAllProducts((prevState) => {
                            return prevState.map((product) => {
                                if (product._id === id) {
                                    return result;
                                }
                                return product;
                            })
                        })
                    })
                    .catch(console.error);
                Swal.fire(
                    'Enabled!',
                    'The product has been enabled.',
                    'success'
                ).then((confirm) => {
                    if (confirm) {
                        // Navigate to the admin dashboard
                        setNavigateToDashboard(true);
                    }
                });
            }
        });
    }

    const handleDisable = (id) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, disable it!'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        isActive: false
                    })
                })
                    .then((response) => response.json())
                    .then((result) => {
                        console.log(result);
                        setAllProducts((prevState) => {
                            return prevState.map((product) => {
                                if (product._id === id) {
                                    return result;
                                }
                                return product;
                            })
                        })
                    })
                    .catch(console.error);
                Swal.fire(
                    'Disabled!',
                    'The product has been disabled.',
                    'success'
                ).then((confirm) => {
                    if (confirm) {
                        // Navigate to the admin dashboard
                        setNavigateToDashboard(true);
                    }
                });
            }
        });
    }

    // If navigateToDashboard is true, redirect to the shop page
    if (navigateToDashboard) {
        return <Navigate to="/shop" />;
    }

    return (
        isLoading ?
            <Loading />
            :
            <>
            <div className="backgroundIan">
                <AdminBanner />
                <br />

                <div className="table-responsive col-sm-12">

            <Container fluid>
                    <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th className="align-middle text-center">Price</th>
                                <th className="align-middle text-center">Availability</th>
                                <th className="align-middle text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {allProducts.map((product) => (
                                <tr key={product._id}>
                                    <td className="align-middle">{product.name}</td>
                                    <td className="align-middle text-left">{product.description}</td>
                                    <td className="align-middle text-center">{product.price}</td>
                                    <td className="align-middle text-center">{product.isActive ? "Available" : "Not Available"}</td>
                                    <td>
                                        <div className="align-middle text-center p-1">
                                            <Link className="btn btn-primary" to={`/updateProduct/${product._id}`}>Update</Link>
                                        </div>
                                        <div className="align-middle text-center p-1">
                                            <Button
                                                variant={product.isActive ? "danger" : "success"}
                                                size='md'
                                                onClick={() => product.isActive ? handleDisable(product._id) : handleEnable(product._id)}
                                            >
                                                {product.isActive ? "Disable" : "Enable"}
                                            </Button>
                                        </div>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Container>
                </div>
            </div>
            </>
    );
}
