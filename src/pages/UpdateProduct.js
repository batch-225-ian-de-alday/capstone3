import {Form, Button, Container} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate, useParams} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

// updating products
export default function UpdateProductView(props) {

    const {productId} = useParams()

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')

    // For determining if button is disabled or not
    const [isActive, setIsActive] = useState(false)

    const navigate = useNavigate()

    // get product details and set to state then edit and update on submit
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/details/${productId}`, {
            method: 'GET',
            headers: {
                'Content-Type' : 'application/json',
            authorization: `Bearer ${localStorage.getItem('token')}`
            },
        })
        .then(response => response.json())
        .then(result => {
            setName(result.name)
            setDescription(result.description)
            setPrice(result.price)
        })
        .catch(console.error)
    }, [])

    function updateProduct(event){  
        event.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type' : 'application/json',
            authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(result => {
    
            if (result) {
                Swal.fire({
                    title: 'Success!',
                    text: 'Product updated successfully',
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            navigate('/adminDashboard')
            
            } else {
                Swal.fire({
                    title: 'Oops!',
                    icon: 'error',
                    text: 'Error updating product'
                })
            }
        })
        .catch(console.error)
    }

    return (
        <div className='backgroundIan' style={{display: 'flex', alignItems: 'flex-start', justifyContent: 'center', height: '100vh'}}>
            <Container className='mt-5 p-4 border bg-dark'>
            <h1 className='text-light'>Update Product</h1>
            <Form onSubmit={updateProduct}>
                <Form.Group>
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="text" placeholder="Enter name" value={name} onChange={(event) => setName(event.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" placeholder="Enter description" value={description} onChange={(event) => setDescription(event.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Price</Form.Label>
                    <Form.Control type="text" placeholder="Enter price" value={price} onChange={(event) => setPrice(event.target.value)} /> 
                </Form.Group>
                <br></br>
                <Button variant="primary" type="submit" disabled={isActive}>
                    Update Product
                </Button>
            </Form>
            </Container>
        </div>
    )
}

