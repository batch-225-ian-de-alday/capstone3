import { Form, Button, Container } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import { useNavigate, Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register() {
	// Activity
	const { user, setUser } = useContext(UserContext)
	const navigate = useNavigate()
	// Activity END


	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNumber, setMobileNumber] = useState('')

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	// For determining if button is disabled or not
	const [isActive, setIsActive] = useState(false)


	function registerUser(event) {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
			.then(response => response.json())
			.then(result => {
				if (result === true) {
					Swal.fire({
						title: 'Oops!',
						icon: 'error',
						text: 'Email already exist! Kindly use another email address.'
					})
				} else {

					fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							mobileNo: mobileNumber,
							email: email,
							password: password1
						})
					})
						.then(response => response.json())
						.then(result => {
							console.log(result);

							setEmail('');
							setPassword1('');
							setPassword2('');
							setFirstName('');
							setLastName('');
							setMobileNumber('');

							if (result) {
								Swal.fire({
									title: 'Registration Successful!',
									icon: 'success',
									text: 'Thank you for registering! You may now login.'
								})

								navigate('/login')

							} else {
								Swal.fire({
									title: 'Registration Failed',
									icon: 'error',
									text: "Please try again. Make sure to input correct details.If the problem persists, please contact the administrator."
								})
							}
						})
				}
			})

	}

	useEffect(() => {
		if ((firstName !== '' && lastName !== '' && mobileNumber.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
			// Enables the submit button if the form data has been verified
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, mobileNumber, email, password1, password2])

	return (
		(user.id !== null) ?
			<Navigate to="/shop" />
			:
			<div className='backgroundIan' style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'center', height: '100vh' }}>
				<Container className='mt-5 p-4 border bg-dark'>
				<Form onSubmit={event => registerUser(event)}>

					<Form.Group controlId="firstName">
						<Form.Label className='labelName'>First Name</Form.Label>
						<Form.Control
							type="text"
							placeholder="Enter First Name"
							value={firstName}
							onChange={event => setFirstName(event.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="lastName">
						<Form.Label className='labelName'>Last Name</Form.Label>
						<Form.Control
							type="text"
							placeholder="Enter Last Name"
							value={lastName}
							onChange={event => setLastName(event.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="mobileNumber">
						<Form.Label className='labelName'>Mobile Number</Form.Label>
						<Form.Control
							type="text"
							placeholder="Enter Mobile Number"
							value={mobileNumber}
							onChange={event => setMobileNumber(event.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="userEmail">
						<Form.Label className='labelName'>Email address</Form.Label>
						<Form.Control
							type="email"
							placeholder="Enter email"
							value={email}
							onChange={event => setEmail(event.target.value)}
							required
						/>
						<Form.Text className="text-muted">
							We'll never share your email with anyone else.
						</Form.Text>
					</Form.Group>

					<Form.Group controlId="password1">
						<Form.Label className='labelName'>Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Password"
							value={password1}
							onChange={event => setPassword1(event.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group controlId="password2">
						<Form.Label className='labelName'>Verify Password</Form.Label>
						<Form.Control
							type="password"
							placeholder="Verify Password"
							value={password2}
							onChange={event => setPassword2(event.target.value)}
							required
						/>
					</Form.Group>
					<br></br>

					{isActive ?
						<Button variant="primary" type="submit" id="submitBtn">
							Submit
						</Button>
						:
						<Button variant="primary" type="submit" id="submitBtn" disabled>
							Submit
						</Button>
					}

				</Form>
				</Container>
   			 </div>
				)
}