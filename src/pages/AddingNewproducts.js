import {Form, Button, Container} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

// adding new products
export default function AddingNewproducts() {
    
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState('')

    // For determining if button is disabled or not
    const [isActive, setIsActive] = useState(false)

    const navigate = useNavigate()

    function addNewProduct(event){
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json',
            authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(result => {
            console.log(result)
            setName('')
            setDescription('')
            setPrice('')

            if (result) {
                Swal.fire({
                    title: 'Success!',
                    text: 'Product added successfully',
                    icon: 'success',
                    confirmButtonText: 'OK'
                })
            navigate('/shop')
            
            } else {
                Swal.fire({
                    title: 'Oops!',
                    icon: 'error',
                    text: 'Error adding product'
                })
            }
        })
        .catch(console.error)
    }

    useEffect(() => {
        if(name && description && price) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, description, price])

    return (

        <div className='backgroundIan' style={{display: 'flex', alignItems: 'flex-start', justifyContent: 'center', height: '95vh'}}>
            <Container className='mt-5 p-4 border bg-dark'>
                    <h1 className="text-center text-light">Create your Product</h1>
                    <Form onSubmit={addNewProduct}>

                        <Form.Group controlId="formBasicName">
                            <Form.Label className='labelName'>Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter name" value={name} onChange={(event) => setName(event.target.value)} />    
                        </Form.Group>
                        <br></br>
                        <Form.Group controlId="formBasicDescription">
                            <Form.Label className='labelName'>Description</Form.Label>
                            <Form.Control type="text" placeholder="Enter description" value={description} onChange={(event) => setDescription(event.target.value)} />
                        </Form.Group>
                        <br></br>
                        <Form.Group controlId="formBasicPrice">
                            <Form.Label className='labelName'>Price</Form.Label>
                            <Form.Control type="text" placeholder="Enter price" value={price} onChange={(event) => setPrice(event.target.value)} />
                        </Form.Group>
                        <br></br>
                        <Button variant="primary" type="submit" disabled={!isActive}>
                            Add Product
                        </Button>
                    </Form>
            </Container>
        </div>
    )
}

