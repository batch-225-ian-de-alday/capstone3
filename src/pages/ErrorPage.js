import { Row, Col, Container as div} from 'react-bootstrap'
import {Link} from 'react-router-dom';


export default function ErrorBanner() {
    return (
        <div fluid className="p-4 text-center backgroundIan text-light">
        <Row>
            <Col>
                <h1>Page Not Found</h1>
                <p>Go back to the <Link to="/">homepage</Link>.</p>
                <img src="https://i.imgur.com/qIufhof.png" alt="404 error"/>
            </Col>
        </Row>
        </div>
    )
}
