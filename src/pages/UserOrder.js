import { useState, useEffect } from 'react';
import Loading from '../components/Loading';
import { Card, Container } from 'react-bootstrap';

export default function UserOrders() {
    const [userOrder, setUserOrder] = useState([]);

    // Add a loading state to handle loading state when fetching data
    const [isLoading, setIsLoading] = useState(true);


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/orders/details`, {
            headers: {
                method: 'GET',
                authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);
                setUserOrder(result);
                setIsLoading(false); // Set loading state to false when data is fetched
            })
            .catch((error) => {
                console.error(error);
                setIsLoading(false); // Set loading state to false when there's an error
                // Display an error message to the user
            });
    }, []);

    // Add a loading indicator to handle when data is being fetched
    if (isLoading) {
        return <Loading />;
    }

    return (
        <>
            <div className='backgroundIan' style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', minHeight: '100vh' }}>
                <Container fluid className='m-5 p-4 border'>
                    {userOrder.map((order, index) => (
                        <Card bg="dark" style={{ marginTop: "1rem", marginBottom: "1.5rem" }} key={index}>
                            <Card.Body className="text-light">
                                <Card.Title>{order.name}</Card.Title>
                                <Card.Title>Orders:</Card.Title>
                                {order.products.map((product, index) => (
                                    <Card style={{ marginTop: "1rem", backgroundColor: "rgb(255,255,255)" }} key={index}>
                                        <Card.Body className="text-dark">
                                            <Card.Subtitle>Product Name:</Card.Subtitle>
                                            <Card.Text>{product.product}</Card.Text>
                                            <Card.Subtitle>Quantity: {product.quantity}</Card.Subtitle>
                                            <Card.Subtitle>Amount: Php:{product.amount.toFixed(2)}</Card.Subtitle>
                                            <br />
                                            <Card.Subtitle>Total: Php:{(product.quantity * product.amount).toFixed(2)}</Card.Subtitle>
                                        </Card.Body>
                                    </Card>
                                ))}
                                <br />
                                <Card.Title>Total Amount:</Card.Title>
                                <Card.Title>Php:{order.totalAmount}</Card.Title>
                            </Card.Body>
                        </Card>
                    ))}
                </Container>
            </div>
        </>
    );
}
