import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import HomePagePicture from "../components/HomePagepicture";
import Footer from "../components/Footer";


export default function Home() {
  return (
    <>
      <div fluid className="backgroundIan ">

        <br></br>
        <HomePagePicture />
        <Banner />
        <Highlights />
        <Footer className="fluid" />
      </div>
    </>
  );
}
