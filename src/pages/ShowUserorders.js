import { useState, useEffect } from 'react';
import Loading from '../components/Loading';
import AllOrders from '../components/AllOrders';

export default function ShowUserorders() {
  const [allOrders, setAllOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/all`, {
      headers: {
        Method: 'GET',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then((response) => response.json())
      .then((result) => {
        setAllOrders(result);
      })
      .catch((error) => {
        console.error(error);
        // Display an error message to the user
      });
  }, []);

  return (
    <div>
      {allOrders.length > 0 ? (
        allOrders.map((order) => <AllOrders key={order._id} orderProp={order} />)
      ) : (
        <Loading />
      )}
    </div>
  );
}