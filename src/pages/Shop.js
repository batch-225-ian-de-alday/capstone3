import ShopCards from "../components/ShopCards";
import { useEffect, useState } from "react";
import Loading from "../components/Loading";
import ProductsTable from "../components/ProductsTable";
// import { useContext } from "react";
// import UserContext from "../UserContext";



export default function Shop() {

    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    // const { user, setUser } = useContext(UserContext);

    // console.log(user);

    useEffect((isLoading) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/active`)
            .then((response) => response.json())
            .then((result) => {
                console.log(result);
                setProducts(
                    result.map((product) => {
                        return (
                            <ShopCards key={product._id} shopProp={product} />
                        )
                    })
                )
                setIsLoading(false);
            })
            .catch(console.error);
    }, []);

    return (

        (isLoading) ?
            <Loading />
            :
            <>
            <div className="backgroundIan">
                {products}
            </div>
            </>
    )
}
