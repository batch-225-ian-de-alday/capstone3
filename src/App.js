import './App.css';
import { useState, useEffect } from 'react'
import { UserProvider } from './UserContext'
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import ErrorPage from './pages/ErrorPage'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import AdminDashboard from './pages/AdminDashboard'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Shop from './pages/Shop';
import AddingNewproducts from './pages/AddingNewproducts';
import UpdateProductView from './pages/UpdateProduct';
import ShowUserorders from './pages/ShowUserorders';
import ProductView from './pages/ProductView';
import UserOrders from './pages/UserOrder';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

  //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {

        // Set the user states values with the user details upon successful login.
        if (typeof data._id !== "undefined") {

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });

          // Else set the user states to the initial values
        } else {

          setUser({
            id: null,
            isAdmin: null
          });

        }

      })

  }, []);


  return (
    <>
      {/*Provides the user context throughout any component inside of it.*/}
      <UserProvider value={{ user, setUser, unsetUser }}>
        {/*Initializes that dynamic routing will be involved*/}
        <Router>
          <AppNavbar />

          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/shop" element={<Shop />} />
            <Route path="/shop/:id" element={<Shop />} />
            <Route path="/addProduct" element={<AddingNewproducts />} />
            <Route path="/updateProduct/:productId" element={<UpdateProductView />} />
            <Route path="/viewProduct/:productId" element={<ProductView />} />
            <Route path="/allOrders" element={<ShowUserorders />} />
            <Route path="/user-orders" element={<UserOrders />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="*" element={<ErrorPage />} />

            {/* // route path for admin dashboard if user is admin */}
            {user.isAdmin ? <Route path="/adminDashboard" element={<AdminDashboard />} /> : null}

          </Routes>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;

